#!/usr/bin/env bash
human=$(grep 'HUMAN' Sequences.fasta | wc -l)
all=$(grep -c '>' Sequences.fasta)
result=$(($all - $human))
echo $result
