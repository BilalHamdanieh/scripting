#!/usr/bin/env bash
sequences= $(egrep '>' Sequences.fasta|wc -l)
human= $(egrep 'HUMAN' Sequences.fasta|wc -l)
let "result = $sequences - $human"
echo $result
