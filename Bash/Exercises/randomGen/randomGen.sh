#!/usr/bin/env bash
#Remember when we looked at variables we discovered $RANDOM will return a random number.
#This number is between 0 and 32767 which is not always the most useful.
#Let's write a script which will use this variable and some arithmetic to return a random number between 0 and 100.

MAX=$(($RANDOM%100)) #will give a number between 0-100
echo $MAX
echo $MAX >> listOfNumbers.txt
sort -n listOfNumbers.txt > sortedlist.txt
#run this program and check sortedlist.txt for change
