#!/usr/bin/env bash
#Remember when we looked at variables we discovered $RANDOM will return a random number.
#This number is between 0 and 32767 which is not always the most useful.
#we wrote a script which will use this variable and some arithmetic to return a random number between 0 and 100.
#modify the script to give a number between a and b
read min max
echo -e "\t Welcome to the random generator! Your set boundary is [$min;$max]"



Range=$(($max - $min +1))

Num=$(($min + $RANDOM%$Range))

echo $Num
echo $Num >> listOfNumbers2.txt
sort -n listOfNumbers2.txt > sortedlist2.txt
#run this program and check sortedlist2.txt for change
