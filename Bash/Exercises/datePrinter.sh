#!/usr/bin/env bash
#Write a Bash script which will print tomorrows data
year=$(date -I|cut -f 1 -d '-')
month=$(date -I|cut -f 2 -d '-')
today=$(date -I|cut -f 3 -d '-'|cut -f 2 -d '0')
tomorrow=$(($today+1))

echo -e "Tomorrow's date is: $year-$month-0$tomorrow (y/m/d)"
