basename -s .TXT -a *.TXT | xargs -n1 -i mv {}.TXT {}.txt
#basename = command to allow us to get the name of the files
# -s to determine the suffix ...
# -a to specify multiple files for basename command
# now it will print the list of files we want to change their suffix
# now we pipe that to the next command with xargs
# xargs will run the command after it on all files
# -n1 = 1 time 
# -i means replace the string
# mv {} ....

