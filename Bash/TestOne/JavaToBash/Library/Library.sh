#!/usr/bin/env bash

echo -e "========\nWelcome to our Library!"
choice=0
num=0
Books=()
number=$(wc -l output.txt | awk ' { print $1 } ')
num=$number
for ((f=0; f <= $number; f++))
do
  line=$(head -n $(($f+1)) output.txt | tail -n 1)
  book=$(echo $line)
  Books[$f]=$book
done
while [ $choice -ne 5 ]
do

echo -e "----\n1. Add Book\n2. Delete Book\n3. List Books\n4. Sort books\n5. Exit and Save the data\n"
read -p "Choice: " choice

case $choice in

  1)
    #Add book
    read -p "Name of the Author: " author
    read -p "Title of the Book: " title
    read -p "Price: " price
    book=$(echo "$title written by $author; price: $price")
    Books[$num]=$book
    num=$(($num+1))
    echo "BOOK ADDED!"
    ;;

  2)
    #Delete Book
    read -p "Enter the title of the book you want to delete: " title
    found=0
    counter=0
    for j in "${Books[@]}"
    do
      arrayTitle=$(echo $j | awk ' { print $1 }')
      if [ $arrayTitle == $title ]
      then

        Books[$counter]=$(echo "${Books[$(($num-1))]}")
        num=$(($num-1))
        echo "$title is Deleted!"
        found=1
      fi
      counter=$(($counter+1))
    done
    if [ $found -eq 0 ]
    then
      echo "No books of given title found"
    fi
    ;;

  3)
    #List Books
    if [ $num -eq 0 ]
    then
      echo "There are no books in the library"
    else
    for ((a=0; a < $num; a++))
    do
      echo -e "${Books[$a]}\n"
    done
    fi
    ;;

  4)
    #Sort Books
    echo -e "1. Alphabetically\t2. By Price (Low to High)"

    read -p "Choice: " sort
    if [ $sort -eq 1 ]
    then
      #sort Alphabetically
      for ((k=0; k < $num; k++))
      do
        echo "${Books[$k]}" >> titles.txt
      done
      sort titles.txt > titles2.txt
        for ((c=0; c <= $num; c++))
        do
          line=$(head -n $(($c+1)) titles2.txt | tail -n 1)
          book=$(echo $line)
          Books[$c]=$book
        done

    elif [ $sort -eq 0 ]
    then
      #sort by Price
      echo "Needs update!"
    else
      echo "Wrong Input, try again:"
    fi
    ;;

  5)
    #Exit
    echo "Exiting..."
    for ((v=0; v < $num; v++))
    do
      echo -e "${Books[$v]}" >> output.txt
    done
    ;;

  *)
    echo "Wrong Input, Try again."
esac

done
