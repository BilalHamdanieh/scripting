#!/usr/bin/env bash
#Write a script in which there will be a menu that can:
#1:combine all the ".txt" files in the current directory
#2:search for a word in a specific file. a- search at the beginning of a line, b- at the end of a line, c- anywhere on the line
#3:count the occurrence of duplicate lines in all the files
#4:exit
#===================

choice=0

while [ $choice -ne 4 ]
do

  echo -e "===============\nMenu:\n1. Combine all the .txt files in the current directory
2. Search for a word in a specific file in the Directory\n3. Count occurrence of duplicate lines in all the files\n4. exit"

  read -p "Choice: " choice

case $choice in

  1)
    exists=$(wc -l *.txt | grep total | awk ' { print $1 }')
    if [ $exists -ge 1 ]
    then
      cat *.txt >> Combined.txt
      echo -e "Done!"
    else
      echo "No .txt files exist in this directory"
    fi
    ;;

  2)
    read -p "Enter the file name: " file
    read -p "Enter the search word: " word

    if [ -e $file ]
    then
      grep "$word" $file | tee output.txt
      we=$(wc -l output.txt | awk ' { print $1 }')
      if [ $we -lt 1 ]
      then
        echo "word doesnt exist"
      fi
    else
      echo "File doesnt exist"
    fi
    ;;

  3)
    exists=$(wc -l *.txt | grep total | awk ' { print $1 }')
    if [ $exists -ge 1 ]
    then
      cat *.txt >> duplicate.txt
      original=$(wc -l duplicate.txt | awk ' { print $1 }')
      after=$(sort duplicate.txt | uniq | wc -l)
      duplicates=$(($original - $after))
      echo -e "There exists $duplicates duplicate lines in all the files in this directory"
      rm duplicate.txt
    else
      echo "No .txt files exist in this directory"
    fi
    ;;

  4)
    echo "Exiting.."
    ;;

  *)
    echo -e "Invalid input, try again..."
    ;;
esac
done
