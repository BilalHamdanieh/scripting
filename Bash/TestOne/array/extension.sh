#!/usr/bin/env bash
##In a directory, output for each extension: the total number of files having this extention, and their total size
#This exercise helps you manipulate arrays in for loops
read -p "Enter the Directory path in you wish to find the number and size of files with specific extentions: " directory
cd $directory
array=(\\.sh \\.txt \\.jpeg \\.jpg \\.R \\.xpm);
length=${#array[@]};

for (( i=0; i < $length; i++))
do
count=$(ls | grep "${array[$i]}" | wc -l)

  if [ $count -ge 1 ]
  then
    size=$(du -ch *${array[$i]} | grep total | awk ' {print $1 } ')
echo "There exists $count ${array[$i]} files for total size $size"
else
  echo "No ${array[$i]} files found in this Directory"
fi
done
