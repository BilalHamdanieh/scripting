#!/usr/bin/env bash
#write a script that returns all prime numbers between 1 and n
read -p "Enter the max bound of your prime numbers list: " n

#all non prime numbers are divisible by 6k-1 or 6k+1 or 2 or 3
i=4
while [ $i -le $n ]
do
if [ $(($i % 2)) -eq 0 ] || [ $(($i % 3)) -eq 0 ]
then
i=$(($i+1))
else
  if [ $i -lt 25 ]
  then
    echo $i
    i=$(($i+1))
  else
  j=5
  square=$(($j*$j))
  while [ $square -le $i ]
  do
    lower=$j #6k-1
    upper=$(($j+2)) #6k+1
    #now check if number is divisble by 6k+1 or 6k-1 for all k's until 6k+1>sqrt(n)
    if [ $(($i % $lower)) -eq 0 ] || [ $(($i % $upper)) -eq 0 ]
    then
    :
    else
    echo "$i --" #problem here
    fi
    j=$(($j+6))
    square=$(($j*$j))
  done
    i=$(($i+1))
  fi
fi
done
#--------
