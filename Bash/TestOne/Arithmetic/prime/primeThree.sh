#!/usr/bin/env bash
prime_function () {
n=$1

if [ $n -le 3 ] && [ $n -gt 1 ]
then
  return "1"
elif [ $(($n % 2)) -eq 0 ] || [ $(($n % 3)) -eq 0 ] #Check if the number is divisible by 2 or 3
then
  return "0" #not prime and exits

else
j=5
while [ $(($j*$j)) -le $n ]
do
  if [ $(($n % $j)) -eq 0 ] || [ $(($n % $(($j+2)))) -eq 0 ] #check if the number is divisible by 6k-1 or 6k+1 for all k where 6k+1<n^2
  then
  return "0" #not prime and exits
  fi
  j=$(($j+6))
done
fi
return "1" # if  it didnt exit before then it is prime for sure
}

read -p "Enter the max boundary of your prime numbers list: " num

i=2

while [ $i -le $num ]
do
  prime_function $i #pass 2...n as an argument to prime_function function
  if [ $? == 1 ] # $? is equivalent to the return of the prime_function
  then
    echo $i
    echo $i >> output1.txt
  fi
  i=$(($i+1))
done
