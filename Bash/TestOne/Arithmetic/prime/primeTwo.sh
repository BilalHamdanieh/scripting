#!/usr/bin/env bash
#write a script that returns all prime numbers between 1 and n
read -p "Enter the max bound of your prime numbers list: " n

for ((number=2 ; number <= $n ; number++))
do
i=2
prime=1
while [ $i -le $(($number/2)) ]
do
if [ $(($number % $i)) -eq 0 ]
then
  prime=0
fi
i=$(($i+1))
done

if [ $prime -eq 1 ]
then
  echo $number
  echo $number >> output2.txt
fi

done
