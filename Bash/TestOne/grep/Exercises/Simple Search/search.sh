#!/usr/bin/env bash
grep "[^a-zA-Z][Tt]he[^a-zA-Z]" sample.txt
#OR:
grep "[^\w][tT]he[^\w]" sample.txt
#OR:
grep \b[Tt]he\b sample.txt
#False Negative: Not showing a hit that it should show
#False positives: Showing a hit that it shouldnt show
