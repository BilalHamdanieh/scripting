#!/usr/bin/env bash
#Search which one of these items is a pc, has more than 6GHz, more than 256GB of space, Cost less than 1000$
#PC should not be Mac or IOS
#First: Pc or Not PC, MAC or IOS > DO NOT WANT
#Word PC may or may not be present!

#Get Any Item with 6+ Ghz: grep -iP "(?<!(\d|\.))([6-9][0-9]*\.*[0-9]*|[0-9]{2,}\.*[0-9]*)\s*\w\s*ghz" (excludes any patterns precceeded by number or decimal point and followed by ghz, while it might have a word between them) (USES PERL REGEX!)

#With the same format, Get Any Item with 256GB+ of Space: grep -iP "(?<!(\d|\.))(2[5-9][6-9][0-9]*\.*[0-9]*|[3-9][0-9]{2,}\.*[0-9]*)\s*\w*\s*gb"

#Any Item with cost less than 1000:(any 3 digits number) grep -iP "(?<!(\d|\.))([0-9]{1,3}(|\.[0-9]*))\s*\w*\s*(\\$|dollars)"

#TRIED BUT FAILED: grep -iPe '(?=.*?((?<!(\d|\.))([6-9][0-9]*\.*[0-9]*|[0-9]{2,}\.*[0-9]*)\s*\w{0,2}\s*ghz)(?=.*?((?<!(\d|\.))(2[5-9][6-9][0-9]*\.*[0-9]*|[3-9][0-9]{2,}\.*[0-9]*)\s*\w{0,2}\s*gb))(?=.*?((?<!(\d|\.))([0-9]{1,3}(|\.[0-9]*))\s*[A-z]{0,2}\s*(\\$|dollars))))' sample.txt

#FIRST METHOD: USING PIPES:
echo -e "Using Pipes and PERL regex:\n"
grep -iP "(?<!(\d|\.))([6-9][0-9]*\.*[0-9]*|[0-9]{2,}\.*[0-9]*)\s*\w*\s*ghz" sample.txt | grep -iP "(?<!(\d|\.))(2[5-9][6-9][0-9]*\.*[0-9]*|[3-9][0-9]{2,}\.*[0-9]*)\s*\w*\s*gb" | grep -iP "(?<!(\d|\.))([0-9]{1,3}(|\.[0-9]*))\s*(\\$|dollars)"
echo -e "================\n"

echo -e "Using Extended Regex and Recursion:"
echo "I could use grep -i -r -E 'PAT1|PAT2|PAT3' But I didnt find anyway to lookbehind in extended regular expressions. This is vital to do since i need to get rid of numbers read from the decimal such as: 5.6124 normal grep for the number without lookbehind will consider (.) to be beginning of the sentence and will therefore select 6124 as my patter as a number bigger than 6."
