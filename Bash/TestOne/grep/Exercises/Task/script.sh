#!/usr/bin/env bash
#Search which one of these items is a pc, has more than 6GHz, more than 256GB of space, Cost less than 1000$
#PC should not be Mac or IOS
#First: Pc or Not PC, MAC or IOS > DO NOT WANT
#Word PC may or may not be present!
#=======================================
#grep with "And" & no order of combination can only be done this way without using perl regex: 123 132 213 231 312 321
#grep -E 'PAT1.*PAT2.*PAT3|PAT1.*PAT3.*PAT2'|PAT2.*PAT1.*PAT3|PAT2.*PAT3.*PAT1|'PAT3.*PAT1.*PAT2|PAT3.*PAT2.*PAT1'
#It is way easier to do: grep -E PAT1 | grep -E PAT2 | grep -E PAT3
#NOTE!!! SHOWING THE FILTERS IS NOT A PART OF THE TASK, BUT IT WAS NICE TO SHOW THEM AND COMPARE THE INTERSECTION of A&B&C combination.
#Get Any Item that is not Mac/IOS/Apple and is PC:
echo "Filtering macs and ios and apple out: "
grep -ivE "(mac|ios|apple)" sample.txt
echo -e "\n====================="
#Get Any Item that has more than 6Ghz cpu speed: search for all numbers >= 6 and followed by Ghz
# [6-9]\.*[0-9]* makes the decimal part optional: 6, 6.41, 6.1512, 6.1241, 9.4124, 8.4124 are inclued
# Must Must be preceeded by a space (to avoid being preceeded by a digit or a period. Cannot use \b since it will look for period as a boundary!
# OR
# Any 2+ digits number is greater than 6
# [0-9]{2,} might be succeeded with a decimal and decimal numbers
# \s* After the number there might be a set of spaces: 6  GHz or it might just be 6Ghz so we use * to indicate 0 to inf.
echo -e "FILTERING THE GHZ:"
grep -iE "(\(|\s)([6-9](\.[0-9]*)?|[0-9]{2,}(\.[0-9]*)?)\s*ghz" sample.txt
echo -e "\n================================"
#==========================================================
#Get Any Item that has more than 258GB of space, greater than 0.258 TB
# 256GB: 2[5-9][6-9] gives all numbers in the range (inclusive) 256-299 here might be a decimal so we make the decimal a possibility
# Any 3+ digit number with first digit [3-9] is greater than 256: [3-9][0-9]{2,} ... There might be a decimal so we make the decimal a possibility
# OR:
#greater than 0.258TB less than 1TB: Same regex but add 0. before it! We can do (0\.)? infront of the current regex, assuming that there is no size of hard disk = 0.258mb
echo -e "Filtering the HD space:"
grep -iE "(\(|\s)(0\.)?((2[5-9][6-9](\.[0-9]*)?|[3-9][0-9]{2,}(\.[0-9]*)?)\s*([gt]b|(gega|tera)byte)|([1-9]*(\.[0-9]*)?)\s*(t|tera)(b|byte))" sample.txt
echo -e "\n===================================="
#==========================================================
#Get any Item that has a cost of less than 1000$
#any number less than 1000$ is any number made up of 1 to 3 digits: [0-9]{1,3}
#Watch for the $ or dollars variations might be $x or $ x or x$ or x $ or xdollars or x dollars
#Assume price cant be < 1
# cant do this because they may both not exist (the dollars signs) grep -iE "(\(|\s)(\$|dollars)?\s*([0-9]{1,3})\s*(\$|dollars)?" sample.txt
# account for commas as they are considered boundaries and can occure in $1,200
echo -e "Filtering the price:"
grep -iE "(\(|\s)([0-9]{1,3}(\.[0-9]*)?)\s*(\\$|dollars)|(\(|\s)(\\$)\s*([0-9]{1,3}(\.[0-9]*)?(\)|\s|[^\,]))" sample.txt
echo -e "\n================================="
#=============================================================
echo -e "Combining All through lookahead: "
grep -iE "(?=(PC))(?=(NVIDIA))" sample.txt
#TRIAL: grep -iE "(?=(\(|\s)([6-9](\.[0-9]*)?|[0-9]{2,}(\.[0-9]*)?)\s*ghz)(?=(\(|\s)(0\.)?((2[5-9][6-9](\.[0-9]*)?|[3-9][0-9]{2,}(\.[0-9]*)?)\s*([gt]b|(gega|tera)byte)|([1-9]*(\.[0-9]*)?)\s*(t|tera)(b|byte)))(?=(\(|\s)([0-9]{1,3}(\.[0-9]*)?)\s*(\\$|dollars)|(\(|\s)(\\$)\s*([0-9]{1,3}(\.[0-9]*)?(\)|\s|[^\,])))(?!=(mac|ios|apple))" sample.txt
echo -e "\nCORRECT: "
grep -iE "(\(|\s)([6-9](\.[0-9]*)?|[0-9]{2,}(\.[0-9]*)?)\s*ghz" sample.txt | grep -iE "(\(|\s)(0\.)?((2[5-9][6-9](\.[0-9]*)?|[3-9][0-9]{2,}(\.[0-9]*)?)\s*([gt]b|(gega|tera)byte)|([1-9]*(\.[0-9]*)?)\s*(t|tera)(b|byte))"| grep -iE "(\(|\s)([0-9]{1,3}(\.[0-9]*)?)\s*(\\$|dollars)|(\(|\s)(\\$)\s*([0-9]{1,3}(\.[0-9]*)?(\)|\s|[^\,]))" | grep -ivE "(mac|ios|apple)"
echo -e "\n=================================="
