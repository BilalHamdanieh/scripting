#!/usr/bin/env bash
#Print all lines that contain a phone number with an extension (the letter x or X followed by four digits)
echo "1st:"
grep -E "[0-9]{3}-[0-9]{3}-[0-9]{4}\s[Xx][0-9]{4}" sample.txt

#Print all lines that begin with three digits followed by a blank. Your answer must use the \{ and \} repetition specifier.
echo -e "\n2nd: "
grep -E ^".{3}\s" sample.txt

#Print all lines that contain a date. Hint: this is a very simple pattern. It does not have to work for any year before 2000.
# unncessary: grep "\s200[0-9]\|\s20[0-9][0-9]" sample.txt
echo -e "\n3rd:"
grep "\b20[0-9][0-9]\b" sample.txt
#OR: grep -E "\s20[0-9]{2}" sample.txt

#Print all lines containing a vowel (a, e, i, o, or u) followed by a single character followed by the same vowel again.
#Thus, it will find “eve” or “adam” but not “vera”. Hint: \( and \)
echo -e "\n4th:"
grep -E '(a|e|i|o|u).\1' sample.txt
# grep -E '20\([0-9]\)\1' sample.txt

#Print all lines that do not begin with a capital S.
echo -e "\n5th:"
grep ^[^S] sample.txt

#Print all lines that contain CA in either uppercase or lowercase.
echo -e "\n6th:"
grep [Cc][Aa] sample.txt
#or: grep -iE "ca" sample.txt

#Print all lines that contain an email address (they have an @ in them), preceded by the line number.
echo -e "\n7th:"
grep -nE "@" sample.txt

#Print first 8 lines that do not contain the word Sep. (including the period).
echo -e "\n8th:"
grep -v "Sep." sample.txt | head -n 8

#Print all lines that contain the word de as a whole word.
echo -e "\n9th:"
grep -iE "\<de\>" sample.txt #use \<whole_word\>
#OR: grep " de " sample.txt
# 20[0-9][0-9]
