#!/usr/bin/env bash

let a= 10+5
echo $a #15

let "a = 5 + 4" #using quotes is to allow spaces and be more readable
echo $a #9

let a++ #incrementing
echp $a #10

let "a = 4 * 5"
echo $a #20

let "a = $1 + 30"
echo $a # 30+ the 3rd command line argument

#==============
#Expr
expr 5 + 4 #correct: prints nine
expr "5 + 4" #prints 5 + 4
# expr 5+4 is wrong
expr 5 \* $1

expr 11 % 2

a=$( expr 10 - 3)
echo $a #7
