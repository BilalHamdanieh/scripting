#!/usr/bin/env bash
#This script, when called must be supplied with 2 arguments, the user will add two arguments and the script will add them

echo -e "The scripts name is:\t $0"


if [ $# -ge 2 ]
then

answer=$(($1+$2))
echo "$answer"

else
  echo "ERROR INVALID ARGS"
fi
