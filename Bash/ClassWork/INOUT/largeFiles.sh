#!/usr/bin/env bash
if [ $# -ne 2 ]
then
  echo -e "Wrong input, must enter two arguments."
  echo -e "\nUsage: $0 \n" >&2
  exit 1
fi


if [ $1 -lt $2 ]
then
  echo "$1 is less than $2"
elif [ $1 -gt $2 ]
then
  echo "$1 is greater than $2"
else
  echo "The two numbers are equal"
fi

echo -n "Write to file and print on Terminal using the tee command: "
echo -n "OUTPUT" | tee IGNORE.txt


echo -n -e "\nNumber of files with size > 10Mb: "
find ~ \
    -type f\
    -size +10M | \
  wc -l
