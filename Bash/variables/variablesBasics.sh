#!/usr/bin/env bash
# A simple variables test
greeting=Hello\ World!
farewell=Bye\ World!

echo -e $greeting "\nI'm Joking!\n"$farewell
#Worked
#---------------------------------------------
#Variables can also be set to directory locations such as:
location=/home/ubuntu/Desktop/Atom/Bash
echo -e "\nHere you will see the storage of the directory given to the variable location:"
ls $location
#----------------------------------------------
#Command Substitution: Allows us to save the output of a command in a variable
var=$(head -n 3 /home/ubuntu/Desktop/Atom/Bash/Codes.txt)
echo -e "\nThe first three lines of the Codes.txt file will be printed:\n"$var
#-----------------------------------------------
#Exporting variables: if you want the variables to be available for other Scripts, you must export the variables as such:
export var
#now var can be used by other Scripts
#------------------------------------------------#END OF VARIABLES FILE 
