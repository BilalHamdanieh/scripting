#!/usr/bin/env bash
read -p "Enter the name of the file/Directory: " path

if [ -f $path ]
then
  echo "is a regular file"
elif [ -d $path ]
then
  echo "is a Directory"
else
  echo "File/Directory doesnt exist"
fi
