#!/usr/bin/env bash

File="text.txt"

if [ ! -e $File ]
then
echo "File Doesnt Exist"
else
if [ -w $File ]
    then
    echo "File is writable"
else [ ! -w $File ]
    echo "File isnt writable"
    chmod u+w $File
    fi
fi
