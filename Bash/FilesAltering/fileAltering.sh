#!/usr/bin/env bash
#This script will extract, manipulate and alter dataSample.txt
#=============================
#Reverse the text:
echo -e "Reversing dataSample.txt: \nOriginal:\n"
cat /home/ubuntu/Desktop/Atom/Bash/FilesAltering/dataSample.txt
echo -e "\n\nNew Reversed using tac [path]: "
tac /home/ubuntu/Desktop/Atom/Bash/FilesAltering/dataSample.txt
#===============================
#Extract the first two Columns and save in list.txt
echo -e "\n\nExtracting the first two Columns using cut -f 1,2 -d ' ' [path]...\n"
cut -f 1,2 -d ' ' /home/ubuntu/Desktop/Atom/Bash/FilesAltering/dataSample.txt

echo -e "\nSaving Result in list.txt...\n"
cut -f 1,2 -d ' ' /home/ubuntu/Desktop/Atom/Bash/FilesAltering/dataSample.txt > /home/ubuntu/Desktop/Atom/Bash/FilesAltering/list.txt
echo -e "\nPrinting list.txt:\n"
cat /home/ubuntu/Desktop/Atom/Bash/FilesAltering/list.txt
#================================
#Numbering the list:
echo -e "\nNumbering list.txt lines using nl -s '<format>' -w <spaces number>[path]"
nl -s ')' -w 1 /home/ubuntu/Desktop/Atom/Bash/FilesAltering/list.txt > listNumbered.txt
cat /home/ubuntu/Desktop/Atom/Bash/FilesAltering/listNumbered.txt
#================================
#search and replace in listNumbered.txt
echo -e "\nReplacing oranges with tomatoes using sed s/search/replace/g and save in tomatoes: "
sed s/oranges/tomatoes/g /home/ubuntu/Desktop/Atom/Bash/FilesAltering/listNumbered.txt > /home/ubuntu/Desktop/Atom/Bash/FilesAltering/tomatoes.txt
echo -e "\Printing tomatoes.txt: "
cat /home/ubuntu/Desktop/Atom/Bash/FilesAltering/tomatoes.txt
#================================
