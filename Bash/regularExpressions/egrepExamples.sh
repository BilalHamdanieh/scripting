#!/usr/bin/env bash
#Printing the original dataSample.txt
echo -e "Printing the Original dataSample.txt:"
cat /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that have at least two letters of [aieo]:"
grep '[aieo]{2,}' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that start with letters in the domain [A-K]"
grep '^[A-K]' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that start with letters in the domain [A-K] or [a-k]"
grep '^[A-K]|^[a-k]' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that contain the segments 'go' or 'on' "
grep 'go|on' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that have 2: "
grep '2.+' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that have 2 at the end: "
grep '2$' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt

echo -e "\nFinding all the lines that contain 2 at the end or the beginning"
grep '2' /home/ubuntu/Desktop/Atom/Bash/regularExpressions/dataSample.txt
#other sytax: grep '2$|2.+'
