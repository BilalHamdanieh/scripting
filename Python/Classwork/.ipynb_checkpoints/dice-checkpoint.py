import random
import sys
# Getting how many times the user wants to roll the dice:
num = input("Please enter how many times you want to roll the dice:\n")

#Declare a sum that is empty:
sum=0;

#For loop to roll:
for x in range(num):
  #Randomize random_face each time entering the loop
  face = random.randint(1,6)
  sum=sum+face

guess = input("Take a guess about the sum: ")

while(guess!=sum):
    if guess>sum:
        guess=input("Guess is high, guess again: ")
    if guess<sum:
        guess=input("Gues is low, guess again: ")
    else:
        sys.exit('You have guessed it!')
