def prime_function(num):
    if ((num <= 3) and (num > 1)):
        return 1
    elif (( num % 2 == 0 ) or ( num % 3 == 0 )):#Check if the number is divisible by 2 or 3
      return 0#not prime and exits
    else:
        j=5
        while (j*j <= num):
            if ((num % j == 0) or (num % (j+2) == 0 )): #check if the number is divisible by 6k-1 or 6k+1 for all k where 6k+1<n^2
                return 0 #not prime and exits
            j=j+6
    return 1 # if  it didnt exit before then it is prime for sure

i=2

while (i <= 1000):
    ret = prime_function(i) #pass 2...n as an argument to prime_function function
    if (ret == 1):
        print ("%03d"%i)
    i=i+1;
