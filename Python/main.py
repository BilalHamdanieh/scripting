import stdio
import stdarray
def main():
	print("Hello")
	with open("sample.txt") as fp:
		count=0 #Tracking the number of
	#Create an initial array of some reasonable size
	any_size = 10
	array = stdarray.create1D(any_size)
	elementAdded = False
	for line in fp:
	    #This for loop will terminate when the fie is all read
	    if (count==len(array)):
	    #Since we need to Read data into the array UNTIL it is full|Resize when full
	    #Resize the array:
	        new_cap = len(array)*2+1
	        temp = stdarray.create1D(new_cap) # New bigger temporary array
	        for k in range(len(array)): # Used to copy all existing elements to temporary array
	            temp[k] = array[k]
	            break #End of Copying
	        array = temp # Call array the new bigger array
	        #array.length = new_cap Capacity of array becomes new Capacity
	    #End of Resizing the new Array
    	#===
 	   #Read Data into the array:
	elementAdded = True
	array[count]=line #Same the element in the array after the last saved element
	print(count)
	count = count + 1 #Increment the count of elements in the array

	#Now Calculate the sum of all the integers saved in the array:
	sum = 0
	for i in range(0,len(array)):
		sum= sum + int(array[i])
	print("The sum of all integers in the file is: " + str(sum))
	#Now Calculate the average, ensuring elements were added (so it doesnt divide by zero)
	if (elementAdded):
	    average=sum/count
	    print("The average is: "+ str(average))
	else:
	    print("The file is empty of integers.")
	  #END OF PROGRAM
if __name__ == "__main__":
	main()
