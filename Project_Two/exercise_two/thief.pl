#!/usr/bin/env perl
#Author: Bilal Hamdanieh; Student ID: 201902645; Email: bilal.hamdanieh@lau.edu
#Request: Dr.Georges Khazen, Scripting class
#Script Brief: Finding the theif: REGEX PERL.
#=================SCRIPT START=========================#
use strict;
use warnings;
#Opening file of data:
open(FH, '<', "data.txt") or die "Download data.txt into the directory and try again!";
#Sort the file:
open(OFH,'>', '.dataOUT.txt') or die $!;

my %seen;
print OFH sort grep !$seen{$_}++, <FH>;

close(OFH);
close (FH);
open(FH, '<', ".dataOUT.txt") or die "Download data.txt into the directory and try again!";
#loop through ever line that represents one potential robber:
while(<FH>){
  #Check if ID's last 4 digits have smae first and last digits: (ID = 1 if true);
  #Student joined after 2017 => 2018|2019|2020
  my $ID = ($_ =~ m/(201[8|9]|2020)(\d)\d{2}\2/);

  #Check if the person lives in dorms:
  my $Dorm = ($_ !~ m/Dorms/);

  #Check if the first name has between 2 and 4 characters:
  #The first name must be a 2-4 characters word followed by space followed by ID number of 8 digits:
  my $first = ($_ =~ m/\b\w{2,4}\b\s\d{8}/);

  #Check if last name has more than 5 characters(not 5 characters):
  #The last name must be either 1 word of 5+ characters or 2 words with 5+ characters at start of line,
  #followed by space followed by first name(Not a number):
  my $word_length_changer = 1;
  my $second_word = 6-$word_length_changer;
  my $last = 0;
  while($word_length_changer<6){
    $last = ($_ =~ m/^\w{6,}|(^\w{1,$word_length_changer}\s\w{$second_word,}\s\D)/);
    if($last==1){
      last;
    }
    $word_length_changer++;
  }

  #Check if they're born on month 10,11,12,1, if they are born before 1975, if their birth day is same digits(11,22):
  my $age = ($_ =~ m/(\d)\1\/(1|10|11|12)\/(19)(7[0-4]|[0-6]\d)/);
  #Summing up all the conditions: Sum must be 5:
  my $condition = $ID+$Dorm+$first+$last+$age;
  if ($condition == 5){
    print $_;
  }
}
#closing file:
close(FH);
