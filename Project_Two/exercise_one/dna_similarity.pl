#!/usr/bin/env perl
#Author: Bilal Hamdanieh; Student ID: 201902645; Email: bilal.hamdanieh@lau.edu
#Request: Dr.Georges Khazen, Scripting class
#Script Brief: Finding Common Nucleotides between two DNA sequences.
#=================SCRIPT START=========================#
use strict;
use warnings;
my $query;
if ($ARGV[0]){
  $query = $ARGV[0];
}
else{
  print "Enter the location of the file of the Query DNA: ";
  $query= <STDIN>;
  # Removes new line from the input
  chomp $query;
}
my $comparable;
if($ARGV[1]){
  $comparable = $ARGV[1];
}
else{
  print "Enter the location of the file of the DNAs you want to compare to the Query: ";
  $comparable = <STDIN>;
  chomp $comparable;
}
#Read file containing query sequesnce:
open (QUERY, '<', "$query") or die "Your DNA File one_dna.fasta isn't found in current directory!";
#Extract DNA sequence/DNA Nucleotides from file:
my $nucleotides = "";
my $description;
my $x = 1;
while(<QUERY>){
  #Save the description line
  if($_=~/^>/){
    $description = $_;
  }
  #Concatenate the DNA sequence
  else{
    $nucleotides = $nucleotides.$_;
  }
}
close(QUERY); #Close file.
#Query DNA sequence and description obtained.
#-----------------
#Read file containing Reverse Translated DNAs:
open (DNAS, '<', $comparable) or die "Your DNA File rev_dna.fasta isn't found in current directory!";
#Obtain Reverse Translated DNAs:
my @dnas = ();
my @frames = ();
my $dna_number = -1;
while(<DNAS>){
  if($_=~/^>/){
    $dna_number++;
    @frames[$dna_number]=$_;
  }
  else{
    no warnings 'uninitialized';
    $dnas[$dna_number]=$dnas[$dna_number].$_;
  }
}
close (DNAS); #Close file.
#-----------------
#Query DNA sequence saved in $nucleotides
#Rev DNA sequences saved in @dna
#-----------------
#Set Arrays to save the number of common Nucleotides:
my @A = (0);
my @G = (0);
my @T = (0);
my @C = (0);
#Compare the Rev DNA sequences to Query DNA sequences:
#for loop to get every DNA to be compared:
for(my $a=0; $a<$#dnas+1; $a++){
  my $dna = $dnas[$a];
  for(my $i=0; $i<length($dna); $i++){
    #Get Nucleotides:
    my $dna_nuc = substr($dna, $i, 1);
    my $que_nuc = substr($nucleotides, $i, 1);
    #Compare:
    if(($dna_nuc eq 'A') && ($que_nuc eq 'A')){
      $A[$a]++;
    }
    elsif(($dna_nuc eq 'G') && ($que_nuc eq 'G')){
      $G[$a]++;
    }
    elsif(($dna_nuc eq 'T') && ($que_nuc eq 'T')){
      $T[$a]++;
    }
    elsif(($dna_nuc eq 'C') && ($que_nuc eq 'C')){
      $C[$a]++;
    }
  }
}
#All info required is Derived
print "Query DNA sequence Description:\n$description\nCheck orderedSequences.fasta for further output!\n";
print ("Common Nucleotides Between Query DNA sequence and Other DNA Sequeces:\n");
my $max_total = 0;
my @total_array = ();
for (my $i=0; $i<$#dnas+1; $i++){
  my $num=$i+1;
  print ("DNA #$num: Description: $frames[$i]");
  print ("Common Nucleotides:\nCommon (A): $A[$i]\nCommon (G): $G[$i]\nCommon (T): $T[$i]\nCommon (C): $C[$i]\n");
  my $total = $A[$i]+$G[$i]+$T[$i]+$C[$i];
  @total_array[$i]=$total;
  print ("Total Common Nucleoties: $total");
  print ("\n=============\n");
}
#=======================
#ORDERING THE SEQUENCES: SORTING ALGORITHM
#@total_array contains the total common Nucleotides number:
my %sorting_hash = ();
my @sorted_indexes = ();
for(my $k=0; $k<$#total_array+1; $k++){
  $sorting_hash{$k}=$total_array[$k];
}
my $iterator = 0;
foreach my $index (sort {$sorting_hash{$a} <=> $sorting_hash{$b} } keys %sorting_hash){
  @sorted_indexes[$iterator]=$index;
  $iterator++;
}
#DNA sorted Acquired.

#Check MOST-COMMON-oer-Nucleotide
print "\nMost-Common-in-DNA-per-Nucleotide:\n";
my $index_of_most_common_A=0;
my $index_of_most_common_G=0;
my $index_of_most_common_T=0;
my $index_of_most_common_C=0;
my $largest_A = 0;
my $largest_G = 0;
my $largest_T = 0;
my $largest_C = 0;
for(my $i=0; $i<$#A+1; $i++){
  #For A:
  if($A[$i] > $largest_A){
    $index_of_most_common_A=$i;
    $largest_A=$A[$i];
  }
  #For G:
  if($G[$i] > $largest_G){
    $index_of_most_common_G=$i;
    $largest_G=$G[$i];
  }
  #For T:
  if($T[$i] > $largest_T){
    $index_of_most_common_T=$i;
    $largest_T=$T[$i];
  }
  #For C:
  if($C[$i] > $largest_C){
    $index_of_most_common_C=$i;
    $largest_C=$C[$i];
  }
}
print "\nDNA with Most Common A is: $frames[$index_of_most_common_A]\n----------";
print "\nDNA with Most Common G is: $frames[$index_of_most_common_G]\n----------";
print "\nDNA with Most Common T is: $frames[$index_of_most_common_T]\n----------";
print "\nDNA with Most Common C is: $frames[$index_of_most_common_C]\n------------------------------------\n";
#PRINTING DNA IN ORDER: IN FASTA FORMAT in orderedSequences.fasta
my $target;
if($ARGV[2]){
  $target = $ARGV[0];
}
else{
  print "Enter the location of the file of the DNAs you want to compare to the Query: ";
  $target = <STDIN>;
  chomp $target;
}
#Outputting info:
#open output file:
open (FW,'>>', $target) or die "$!";
@sorted_indexes = reverse @sorted_indexes;
for(my $ind=0; $ind<$#dnas+1; $ind++){
  print FW ("$frames[$sorted_indexes[$ind]]"."(A): $A[$sorted_indexes[$ind]]  (G): $G[$sorted_indexes[$ind]] (T): $T[$sorted_indexes[$ind]]  (C): $C[$sorted_indexes[$ind]]  (total): @total_array[$sorted_indexes[$ind]]\n");
  print FW "$dnas[$sorted_indexes[$ind]]";
}
close (FW);
