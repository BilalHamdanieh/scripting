#!/usr/bin/env perl
#Translation - Transcription
#DNA.txt contains the DNA sequences
use strict;
use warnings;
#read file
open (data, "<ACE2Sequences.fasta") or die "DNA file data doesn't exist";

#Extracting the DNA sequence from the fasta file:
my @dna = ();
my @discription = ();
my $dna_number=-1;
while (<data>){
    if ($_ =~ /^>/) {
      my $dna_number++;
      my  @discription[$dna_number] = split(' ',$_);
    }
    else{
        my @dna[$dna_number]=@dna[$dna_number].$_;
    }
}
#closing file
close (data) or die "File couldn't close!";
#get every DNA sequence
for (my $a = 0 ; $a < $#dna + 1; $a++) {
    #Transcription:
    my $rna = "";
    for (my $i = 0; my $i<length(@dna[$a]); my $i++){
      #save the @nucleotides in an array, just in case they need to be used later on
      my @nucleotides[$i]= substr(@dna[$a], $i, 1);
      if (@nucleotides[$i] eq 'A'){
        $rna = $rna.'U';
      }
      elsif (@nucleotides[$i] eq 'T'){
        $rna = $rna.'A';
      }
      elsif (@nucleotides[$i] eq 'G'){
        $rna = $rna.'C';
      }
      elsif (@nucleotides[$i] eq 'C'){
        $rna = $rna.'G';
      }
    }
    #Now the RNA is acquired.
    #Genetic code table:
my %genetic_code_table = (
    'UUU' => 'F',
    'UUC' => 'F',
    'UUA' => 'L',
    'UUG' => 'L',
    'CUU' => 'L',
    'CUC' => 'L',
    'CUA' => 'L',
    'CUG' => 'L',
    'AUU' => 'I',
    'AUC' => 'I',
    'AUA' => 'I',
    'AUG' => 'M',
    'GUU' => 'V',
    'GUC' => 'V',
    'GUA' => 'V',
    'GUG' => 'V',
    'UCU' => 'S',
    'UCC' => 'S',
    'UCA' => 'S',
    'UCG' => 'S',
    'CCU' => 'P',
    'CCC' => 'P',
    'CCA' => 'P',
    'CCG' => 'P',
    'ACU' => 'T',
    'ACC' => 'T',
    'ACA' => 'T',
    'ACG' => 'T',
    'GCU' => 'A',
    'GCC' => 'A',
    'GCA' => 'A',
    'GCG' => 'A',
    'UAU' => 'Y',
    'UAC' => 'Y',
    'UAA' => '.',
    'UAG' => '.',
    'CAU' => 'H',
    'CAC' => 'H',
    'CAA' => 'Q',
    'CAG' => 'Q',
    'AAU' => 'N',
    'AAC' => 'N',
    'AAA' => 'K',
    'AAG' => 'K',
    'GAU' => 'D',
    'GAC' => 'D',
    'GAA' => 'E',
    'GAG' => 'E',
    'UGU' => 'C',
    'UGC' => 'C',
    'UGA' => '.',
    'UGG' => 'W',
    'CGU' => 'R',
    'CGC' => 'R',
    'CGA' => 'R',
    'CGG' => 'R',
    'AGU' => 'S',
    'AGC' => 'S',
    'AGA' => 'R',
    'AGG' => 'R',
    'GGU' => 'G',
    'GGC' => 'G',
    'GGA' => 'G',
    'GGG' => 'G',
  );
    #Translation:
    my $frame=0;
    while($frame<3){
      my $rna_framed=substr($rna,$frame,length($rna));
      my $p=0;
      for (my $i=0; $i<length($rna_framed);$i=$i+3){
        $codon = substr($rna_framed, $i, 3);
        @protein[$p]=$genetic_code_table{$codon};
        $p++;
      }
      my $frame++;
      my $num=$a+1;
      print "Protein of DNA #$num and frame $frame: @protein\n"
    }
    #protein array print:

}
