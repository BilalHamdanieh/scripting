#!/usr/bin/env perl
#Convey Format:
print "FORMAT: Gly-Glu-Asp-Val-Asp-----End\n";

#Read Sequence:
print "Enter your Protein/Peptide Sequence to reverse Translate: ";
$seq = <STDIN>;
chomp $seq;

#Split sequence into seperate amino acids
@amacid = split ('-', $seq);

#hash from amino acid to corresponding codon
%rev_table = (
  'Ala' => ['GCA','GCC','GCG','GCT'],
  'Arg' => ['AGA','AGG','GGA','CGC','CGG','CGT'],
  'Asn' => ['AAC','AAT','GAC','GAT'],
  'Cys' => ['TGC','TGT'],
  'End' => ['TAA','TAG','TGA'],
  'Gln' => ['CAA','CAG'],
  'Glu' => ['GAA','GAG'],
  'Gly' => ['GGA','GGC','GGG','GGT'],
  'His' => ['CAC','CAT'],
  'Ile' => ['ATA','ATC','ATT'],
  'Leu' => ['CTA','CTC','CTG','CTT','TTA','TTG'],
  'Lys' => ['AAA','AAG'],
  'Met' => ['ATG'],
  'Phe' => ['TTC','TTT'],
  'Pro' => ['CCA','CCC','CCG','CCT'],
  'Ser' => ['AGC','AGT','TCA','TCC','TCG','TCT'],
  'Thr' => ['ACA','ACC','ACG','ACT'],
  'Trp' => ['TGG'],
  'Tyr' => ['TAC','TAT'],
  'Val' => ['GTA','GTC','GTG','GTT'],
);
#Crossing the possibilities function: a b c   1 2 3
sub combination
{
  #arguments are two arrays:
  @original=@$_[0];
  @new=@$_[1];
  @combinations = ();
  for ($o=0; $o<$#original+1; $o=$o+1){
      for($n=0; $n<$#new+1; $n=$n+1){
        push(@combinations,@original[$o].@new[$n]);
      }
  }
  return @combinations;
}

$original_ref = $rev_table{@amacid[0]};
for ($i=1; $i<$#amacid+1; $i=$i+1){
  $new_ref = $rev_table{@amacid[$i]};
  @original = combination( $original_ref, $new_ref);
}

for ($p=0; $p<$#original+1; $p=$p+1){
  $number=$p+1;
  print "Possibilty $number: ".@original[$p]."\n";
}
