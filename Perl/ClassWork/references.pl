#!/usr/bin/env perl

#Importance of referencing:
#Allocate the variables in sequence in memory in order not
#to jump from one position to another when searching for the variables

print "enter your number: ";
$num1 = <STDIN>;
chomp $num1;

$reference = \$num1;

print "original: $num1\n"; #100
print "regerence: $reference\n"; #location
print "dereference: $$reference\n"; # equivalent to num1

print "Change your value using dereference: ";
$$reference=<STDIN>; #changing the value
chomp $$reference;

print "After chaning the value: \n";

print "original: $num1\n"; #will print new value
print "regerence: $reference\n"; #locaiton (original as well)
print "dereference: $$reference\n"; #print new value

$ref_of_ref = \$reference;
print "\n=========\nReference of reference: $ref_of_ref";
print "\nValue of reference: $$ref_of_ref";
print "\nValue of reference of reference: $$$ref_of_ref\n";
