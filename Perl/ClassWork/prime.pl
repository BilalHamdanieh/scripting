#!usr/bin/perl
sub prime_function ($) {
  $n=@_[0];
  if (($n <= 3) && ($n > 1))
  {
      return ("1");
  }


  elsif (( $n % 2 == 0 ) || ( $n % 3 == 0 ))#Check if the number is divisible by 2 or 3
  {
      return ("0");#not prime and exits
  }


  else{
    $j=5;
    while ( $j*$j <= $n ){
      if (($n % $j == 0) || ($n % ($j+2) == 0 )) #check if the number is divisible by 6k-1 or 6k+1 for all k where 6k+1<n^2
      {
        return ("0"); #not prime and exits
      }
      $j=$j+6;
    }
  return ("1"); # if  it didnt exit before then it is prime for sure
  }
}

$i=2;

while ($i <= 1000) {
$ret = prime_function($i); #pass 2...n as an argument to prime_function function
$ints = int($ret);
if ($ints == 1)
{
  printf "%03d", $i;
  print "\n"
}
$i=$i+1;
}
