#!usr/bin/perl

print "Welcome to Lebanon!\n==========\n";

%country = qw (
      Beirut 0000
      Jbeil 1000
      Marj 1100
      Tripoli 1110
      Sour 1111
);
print "Cities:\n";

foreach $city (keys %country){
  print "$city\n";
}

print "\nEnter a city name to get its Zip Code: ";
$choice= <STDIN>;
# Removes new line from the input
chomp $choice;
$found = 0;
while (($city, $zipcode) = each(%country)){
  if ($choice eq "$city"){
    print "Your City and Zip Code = $city : $zipcode\n";
    $found=1;
  }
}

if ($found == 0){
  print "City not registered.";
}

print "\n---REVERSING HASH---\n";
reverse %country;
print "Enter a Zip Code to find the corresponding City: ";
$zip = <STDIN>;
chomp $zip;
$found =0;
while (($city, $zipcode) = each(%country)){
  if ($zip eq "$zipcode"){
    print "Your City and Zip Code = $city : $zipcode\n";
    $found=1;
  }
}

if ($found == 0){
  print "No city with $zip is found.";
}
