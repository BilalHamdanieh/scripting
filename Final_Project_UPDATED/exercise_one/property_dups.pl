#!/usr/bin/env perl
#Final Project; Student: Bilal Hamdanieh, Student ID: 201902645
#Project: Removing Duplicates
#Script Language: Perl, Called by a Bash Script.
#=====================START OF SCRIPT=====================#
use strict;
use warnings;
#uniqueInteractions.txt contains half way filtered data.
#The purpose of this script is to Filter for Property 2: Motif_A of X = Motif_B of Y
#Approach: Previously removed Duplicates: proteinX: Motif_A + OTHERS  Motif_B + OTHERS
#                                         proteinY: Motif_A + OTHERS  Motif_B + Ohters
#          Protein X Data = Protein Y Data => Removed Duplicate.
#Case: if in Protein X: Motif_A = Motif_B and Motif_A of PX = Motif_B of PY then 2nd Property is Equivalent to 1st Property
#Therefore: The approach consists of:
#1. Check if Motif_A = Motif_B: if TRUE then Skip, if FALSE then check for potential Duplicates
#2. Check For Potential Duplicates: Span each line => if line is unique add it to a list, if not, print it duplicateInteractions.txt

#Open Transmembrane file for reading:
open (UNIQ, "<", "TransmembraneData.txt") or die "TransmembraneData.txt isn't found. Run The Bash Script.";
#Open duplicateInteractions file for appending:
open(DUPS, ">>", "duplicateInteractions.txt") or die "duplicateInteractions.txt isn't found. Run the Bash Script";
#Open uniqueInteractions file for appending:
open (UNIQWR, ">>", "uniqueInteractions.txt") or die "uniqueInteractions.txt isn't found. Run The Bash Script.";
#Create Array to Store Unique Interactions:
#my @uniq = ();
#Create Array for Reference of Unique Interactions: (properties stored here only)
my @props = ();
#Read uniqueInteractions line by line:
my $count0 = 0;
my $count1 = 0;
my $count2 = 0;
while(<UNIQ>){
    my $interaction = $_;
    #Get the Data to Compare: (Motif, Error Cost, Cost)
    my @data_list = split("\t", $interaction);
    #Both A-B and B-A are extracted for each interaction:
    #UPDATE: Protein ID Taken into Consideration: Even if A-B = B-A, it is not a duplicate if it is in a different protein!
    my $data = $data_list[1].$data_list[4].$data_list[7].$data_list[8].$data_list[10].$data_list[13].$data_list[16].$data_list[17];
    my $data_v2 = $data_list[10].$data_list[13].$data_list[16].$data_list[17].$data_list[1].$data_list[4].$data_list[7].$data_list[8];

    #Case: A-B = B-A then it is already checked for. Therfore Procceed only if they arent equal. This will exclude about 40 Interactions (100% unique).
    #Check if A-B or B-A exists in the reference Array props:
    #if the reference already contains the interaction => Add it to duplicateInteractions
      if ((grep {$_ eq $data} @props) || (grep {$_ eq $data_v2} @props)){
        print DUPS $interaction;
        $count1++;
      }
      #In Case the Interaction is Unique: Save it to the uniq array & to reference:
      else{
        print UNIQWR $interaction;
        $count2++;
        push(@props, $data);
        push(@props, $data_v2);
      }
      $count0++;
}
close (UNIQWR);
close (DUPS);
close (UNIQ);
print "The Total Number of Interactions is: ".$count0."\n".
"The Total Number of Unique Interactions is: ".$count2."\n".
"The Total Number of Duplicate Interactions is: ".$count1;
