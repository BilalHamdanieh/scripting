#THE FOLDER EXERCISE_TWO CONTAINS EXERCISE 2 SCRIPTS & DATA
-----------------------------------------------
====DO NOT READ HERE! THIS HAS BEEN UPDATED====READ BELOW THE NAME====
Script 1: MAIN SCRIPT: removing_dups.sh; Handles Property 1 of Duplicate Interactions Using Bash awk,sort,uniq commands.
Script 2: Called From MAIN SCRIPT: property_dups.pl (hidden file); Handles Property 2 of Duplicate Interactions Using Perl File Reading + Lists/Arrays

Input File: TransmembraneData.txt
Output Files: duplicateInteractions.txt (contains duplicates) && uniqueInteractions.txt (contains unique interactions).

NOTE Run Script 1 For the Correct

=========================================
#Final Project; Student: Bilal Hamdanieh, Student ID: 201902645
#Project: Removing Duplicate Interactions
#Scripts Languages: Bash + Perl Script Call
=========================================
UPDATE:
~~ ONLY RUN THE PERL SCRIPT! ~~ BASH SCRIPT IS ONLY USEFULL IN CASE MANY IDENTICAL LINES ARE FOUND AND HAS BEEN COMMENTED AND HIDDEN (.removing_dups.sh).
