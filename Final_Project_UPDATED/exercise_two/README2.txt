#THE FOLDER EXERCISE_ONE CONTAINS EXERCISE 1 SCRIPTS & DATA
-----------------------------------------------

Script 1: MAIN SCRIPT: atoms_int.sh; Handles Identical Duplicates (A-B && A-B) where A= AminoAC-Res_no. etc.
Script 2: Called From MAIN SCRIPT: .perl_script (hidden file); Handles A-B && B-A Duplicates, Counts the Unique, Prints Table

Files Input: PDBSumFiles/*.txt
File Output: interactionStatistics.txt; Contains numbers of interactions in table form: This File is Tab Delimeted.

NOTE Run Script 1 For the Correct Output

=========================================
#Final Project; Student: Bilal Hamdanieh, Student ID: 201902645
#Project: Counting Interactions
#Scripts Languages: Bash + Perl Script Call
=========================================
