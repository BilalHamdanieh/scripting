#!/usr/bin/env bash
#Final Project; Student: Bilal Hamdanieh, Student ID: 201902645
#Project: Counting Protein Interactions
#Script Language: Bash + Perl Script Call
#=====================START OF SCRIPT=====================#
#1. Merge all data required from txt files into data.txt:
#Approach: For loop + Sed from pattern1 to pattern2 + sed to remove titles + append to data.txt
#                   + awk to extract important data only.
i=0 #to get the count of files
for file_loc in PDBSumFiles/*.txt
do
i=$(($i+1))
cat $file_loc | sed -n '/^Non-bonded/,/^Number/p' |\
                sed -n '/^Non-bonded/,/^Disulphide/p' |\
                sed -n '/^Non-bonded/,/^Salt/p' |\
                sed '$d' | sed -e '1,7d'|\
                awk '{ print $4"\t"$5"\t"$10"\t"$11 }' >> .data.txt.temp
done
echo "Count of Files Proccessed: $i"
#Since AminoAC,N1 - AminoAC2,N2 is only count as one => Remove Exact Duplicates. (A-B and B-A duplicates still need to be remoed)
sort .data.txt.temp | uniq >> data.txt
rm .data.txt.temp
#DATA IS ACQUIRED: in data.txt
#=================
#Call pirl script:
perl .perl_script.pl
#Prompt the user to whether Print the file or Not:
echo "Interactions Statistics Are Now Available in interactionStatistics.txt"
read -p "Do you want to check them? (y/n): " check
if [ $check == "y" ]
then
  cat interactionStatistics.txt
else
  echo "You can always check them using: cat interactionStatistics.txt on command line."
fi
#===Remove unneccessary files:
rm data.txt
