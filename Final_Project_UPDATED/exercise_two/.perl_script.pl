#!/usr/bin/env perl
use warnings;
use strict;

open(ATOMS, "<","data.txt");
#Save content of file in Array @lines
my @lines = <ATOMS>;
#Remove Blank Lines in case there are any:
@lines = grep (!/^\s/, @lines);
#Loop through each line to build a hash for interactions:
my %ints = ();
#Upadate: Array to Collect for A1 N1 - A2 N2 repeated as A2 N2 - A1 N1
my @uniq_aa = ();
foreach my $line(@lines) {
    #Build AminoAc - AminoAc Interaction + Get Numbers N1 - N2
    #Split line into column-fields:
    my @aa = split("\t",$line);

    my $a_a = $line;
    #B-A Version
    my $a_a_v2;
    #Note: Interaction A-B is same as B-A => We will add them later!

    #Building the Hash:
    #Approach: if a_a is Unique => Add it to Hash as Key with Value = (1) where 1 = #occurances
    #          if a_a is Not Unique, just add +1 to Value
    #Note: It is impossible at this stage to Find A1 N1 - A2 N2 More than once
    #However: It is possible to find: A1 N1 - A2 N2 repeated as A2 N2 - A1 N1
    # => New Approach: Remove Duplicates First then Construct the Hash then Add the Values.

    #Case: A-B = B-A then it is already checked for.
    my $b = $aa[2]."\t".$aa[3];
    chop $b;
    my $a = $aa[0]."\t".$aa[1]."\n";
    chop $a;
    $a_a_v2 = $b."\t".$a."\n";
    #print $a_a_v2;
    #print $a_a;
    if($a_a ne $a_a_v2){
      #Check if A-B or B-A exists in the reference uniq_aa
      #Only if the reference doesnt already contains the interaction => push interaction
      if ((grep {$_ eq $a_a} @uniq_aa) || (grep {$_ eq $a_a_v2} @uniq_aa)){
          #print $a_a; #Uncomment to check if its doing the right job
          ;
      }
      else{
        push(@uniq_aa, $a_a);
      }
    }
}
#Now the Array @uniq_aa contains only uniq interactions:
#Counting Interactions and Constructing Hash:
foreach my $int(@uniq_aa){
  #Now interactions can be count directely disregarding the Res no.
  my @int_keys = split("\t",$int);
  my $int_key = $int_keys[0]."-".$int_keys[2];
  #if int_key exists in hash: add to its value + 1 =>
    if (exists $ints{$int_key}){
      #print $int_key."\n"; #to check if correct => checked
      my $prev_value = $ints{$int_key};
      my $new_value = $prev_value+1;
      #update hash:
      $ints{$int_key} = $new_value;
    }
  #if int_key doesnt exist in hash: add it to hash with original value=1;
   else{
     $ints{$int_key} = 1;
  }
}
open(TBW, ">>", "interactionStatistics.txt") or die "interactionStatistics.txt Not Found.";
open(TB, "<", ".table_form.txt") or die "table_form.txt hidden file isnt downloaded!";

#Get header from table_form:
my $header = <TB>;
#write header to interactionStatistics.txt
print TBW $header;
#Form Array of Amino Acids:
my @ac = split("\t",$header);

while(<TB>){
  #Get the first Amino Acid
  my $first_aa = $_;
  chop $first_aa;
  #For the output to interactionStatistics.txt
  my $output = $_;
  chop $output;
  #Form combinations: $first_aa-$ac[index] (A-B and B-A)
  for(my $index=1; $index<$#ac+1; $index++){
    my $comb1 = $first_aa."-".$ac[$index];
    my $comb2 = $ac[$index]."-".$first_aa;
    my $count1=0;
    my $count2=0;
    #Problem with extra '\n' at the last amino acid fixed:
    if($index==$#ac){
      my $sec_aa = $ac[$index];
      chop $sec_aa;
      $comb1 = $first_aa."-".$sec_aa;
      $comb2 = $sec_aa."-".$first_aa;
    }
    #Get the Count of each combination version:
    if (exists $ints{$comb1}){
      $count1=$ints{$comb1};
    }
    if(exists $ints{$comb2}){
      $count2=$ints{$comb2};
    }
    #Sum the count:
    my $total = $count1+$count2;
    #Concat to output
    $output = $output."\t".$total;
  }
    print TBW $output."\n";
}

#Cloes files...
close (TB);
close(TBW);
close(ATOMS);
