#!/usr/bin/env bash
#Final Project; Student: Bilal Hamdanieh, Student ID: 201902645
#Project: Removing Duplicates
#Script Language: Bash + Perl Script Call
#=====================START OF SCRIPT=====================#

#1. Printing total Number Of Interactions: Input $1 = TransMembrane.txt
if [ -z "$1" ]
then
	wrong_input=1 #Wrong Input = True
	while [ $wrong_input == 1 ]
	do
		read -p "Please Enter the Data File Location: " file_loc
		if [ -e $file_loc ]
		then
			wrong_input=0
			echo -e "The Total Number of Interactions is: $(wc -l $(echo $file_loc | cut -d "." -f1).txt | awk '{ print $1 }')"
		else
			echo -e "File Entered is Invalid, Try Again...\n"
		fi
	done
else
	echo -e "The Total Number of Interactions is: $(($(wc -l $(echo $1 | cut -d "." -f1).txt | awk '{ print $1 }')-1))"
fi
#========================================End of First Task

#2.	Dealing with Duplicate Interactions: Identical Dups, Property-Based Dups
#	While dealing with duplicates:
#				i.Add the duplicates to: duplicateInteractions.txt
#			   ii.Add the unique Interactions to uniqueInteractions.txt
#======================
#	Identical Dups:
#	Seperate Identical Lines Duplicates and Unique Lines into their respective txt Files:
#Method: Sort File | Redirect Output to Two Commands: get Duplicates & get Unique (>/dev/null to stop from printing on screen)
#======================
#After Indentical Duplicates Have Been Filtered... Procceed to Property Based Duplicates...
#	Property: Interact(X) = Interact(Y) if Motif_A(X)<=>Motif_B(Y) & Error_rate & Cost
#	(Motif_A = Motif_A && Motif_B = Motif_B (P1)) || (Motif_A = Motif_B && Motif_B = Motif_A (P2))
#Dealing with (P1): + Same for Error_rate and Cost (Columns 5,8,9 = 14,17,18 => Remove Duplicates): Using the Famous Awk Method
#====================== #UPDATE: Protein ID Must be taken into Consideration: Column 2 must be same => Duplicates, If not then Unique
#Adding the titles:
header=$(head -n 1 TransmembraneData.txt)
echo "$header" > duplicateInteractions.txt
#Adding Both Together: The result pipeline is:
sed 1d TransmembraneData.txt | sort TransmembraneData.txt | tee >(uniq -D >> duplicateInteractions.txt) >(tee >(awk -F "\t" '!seen[$2,$5,$8,$9,$14,$17,$18]++' >> uniqueInteractions.txt) >(awk -F "\t" 'seen[$2,$5,$8,$9,$14,$17,$18]++' >> duplicateInteractions.txt) >/dev/null) >/dev/null
#======================
#One more property to deal with: (P2): Done in Perl:
#Data is now filtered from about 43000 interactions to 162 interactions:
#Check in Perl: Motif_A = Motif_B && Motif_B = Motif_A (also for Error_rate & Cost) <=> (A INT B) && (B INT A) are Duplicates
perl .property_dups.pl
#Add header to uniqueInteractions:
echo "$header" > temp.txt
#Delete header from uniqueInteractions
grep -v "organism" uniqueInteractions.txt >> temp.txt
mv temp.txt uniqueInteractions.txt

echo "Number of Duplicate Interactions Found: $(($(wc -l duplicateInteractions.txt|cut -d " " -f1) -1))"
echo "Number of Unique Interactions Found: $(($(wc -l uniqueInteractions.txt|cut -d " " -f1) -1))"
